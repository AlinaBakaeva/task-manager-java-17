package com.bakaeva.tm.api.service;

import com.bakaeva.tm.enumerated.Role;

public interface IAuthService {

    String getUserId();

    void login(String login, String password);

    void checkRoles(Role[] roles);

    boolean isAuth();

    void logout();

    void registry(String login, String password, String email);

}