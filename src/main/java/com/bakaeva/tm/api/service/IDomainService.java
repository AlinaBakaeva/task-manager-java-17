package com.bakaeva.tm.api.service;

import com.bakaeva.tm.dto.Domain;

public interface IDomainService {

    void load(Domain domain);

    void export(Domain domain);

}
