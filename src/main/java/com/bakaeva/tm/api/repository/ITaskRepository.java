package com.bakaeva.tm.api.repository;

import com.bakaeva.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {

    void add(String userId, Task task);

    void add(Task task);

    void remove(String userId, Task task);

    void clear(String userId);

    void clear();

    void addAll(List<Task> taskList);

    void load(List<Task> taskList);

    List<Task> findAll();

    List<Task> findAll(String userId);

    Task findById(String userId, String id);

    Task findByIndex(String userId, Integer index);

    Task findByName(String userId, String name);

    Task removeById(String userId, String id);

    Task removeByIndex(String userId, Integer index);

    Task removeByName(String userId, String name);

}