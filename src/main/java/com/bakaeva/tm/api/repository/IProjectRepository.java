package com.bakaeva.tm.api.repository;

import com.bakaeva.tm.entity.Project;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    void add(String userId, Project project);

    void addAll(List<Project> projectList);

    void load(List<Project> projectList);

    void remove(String userId, Project project);

    void clear(String userId);

    void clear();

    List<Project> findAll();

    List<Project> findAll(String userId);

    Project findById(String userId, String id);

    Project findByIndex(String userId, Integer index);

    Project findByName(String userId, String name);

    Project removeById(String userId, String id);

    Project removeByIndex(String userId, Integer index);

    Project removeByName(String userId, String name);

}