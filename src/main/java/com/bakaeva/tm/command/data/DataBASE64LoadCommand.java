package com.bakaeva.tm.command.data;

import com.bakaeva.tm.api.service.IDomainService;
import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.constant.DataConstant;
import com.bakaeva.tm.dto.Domain;
import com.bakaeva.tm.enumerated.Role;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.ObjectInputStream;
import java.nio.file.Files;

public class DataBASE64LoadCommand extends AbstractCommand {

    @Override
    public String name() {
        return "data-base64-load";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Load data from base64 binary file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 LOAD]");
        final File file = new File(DataConstant.FILE_BASE64);
        final byte[] fileBytes = Files.readAllBytes(file.toPath());
        final String base64 = new String(fileBytes);
        final byte[] bytes = new BASE64Decoder().decodeBuffer(base64);
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        final Domain domain = (Domain) objectInputStream.readObject();
        final IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);
        objectInputStream.close();
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}