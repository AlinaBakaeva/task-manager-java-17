package com.bakaeva.tm.command.user;

import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.entity.User;
import com.bakaeva.tm.util.TerminalUtil;

public class PasswordUpdateCommand extends AbstractCommand {

    @Override
    public String name() {
        return "password-update";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Update my password.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        final String password = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().updatePasswordById(userId, password);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

}