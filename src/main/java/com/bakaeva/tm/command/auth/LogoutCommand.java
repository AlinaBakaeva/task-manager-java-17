package com.bakaeva.tm.command.auth;

import com.bakaeva.tm.command.AbstractCommand;

public class LogoutCommand extends AbstractCommand {

    @Override
    public String name() {
        return "logout";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Log out.";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        serviceLocator.getAuthService().logout();
        System.out.println("[OK:]");
    }

}