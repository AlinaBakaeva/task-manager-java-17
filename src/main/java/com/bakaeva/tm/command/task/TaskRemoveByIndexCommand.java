package com.bakaeva.tm.command.task;

import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.entity.Task;
import com.bakaeva.tm.util.TerminalUtil;

public class TaskRemoveByIndexCommand extends AbstractCommand {

    @Override
    public String name() {
        return "task-remove-by-index";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Remove task by index.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = serviceLocator.getTaskService().removeByIndex(userId, index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}