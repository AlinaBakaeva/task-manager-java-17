package com.bakaeva.tm.command.task;

import com.bakaeva.tm.command.AbstractCommand;
import com.bakaeva.tm.entity.Task;
import com.bakaeva.tm.util.TerminalUtil;

public class TaskViewByNameCommand extends AbstractCommand {

    @Override
    public String name() {
        return "task-view-by-name";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Show task by name.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findByName(userId, name);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
    }

}