package com.bakaeva.tm.command.system;

import com.bakaeva.tm.command.AbstractCommand;

public class VersionCommand extends AbstractCommand {

    @Override
    public String name() {
        return "version";
    }

    @Override
    public String argument() {
        return "-v";
    }

    @Override
    public String description() {
        return "Show version info.";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.0.15");
    }

}