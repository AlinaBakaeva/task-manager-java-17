package com.bakaeva.tm.exception.empty;

public class EmptyMiddleNameException extends RuntimeException {

    public EmptyMiddleNameException() {
        super("Error! Middle Name is empty...");
    }

}