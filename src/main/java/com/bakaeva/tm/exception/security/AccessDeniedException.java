package com.bakaeva.tm.exception.security;

public class AccessDeniedException extends RuntimeException {

    public AccessDeniedException() {
        super("Error! Access Denied...");
    }

}