package com.bakaeva.tm.repository;

import com.bakaeva.tm.api.repository.IProjectRepository;
import com.bakaeva.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(final Project project) {
        projects.add(project);
    }

    @Override
    public void add(final String userId, final Project project) {
        project.setUserId(userId);
        projects.add(project);
    }

    @Override
    public void addAll(final List<Project> projectList) {
        projects.addAll(projectList);
    }

    @Override
    public void load(final List<Project> projectList) {
        clear();
        addAll(projectList);
    }

    @Override
    public void remove(final String userId, final Project project) {
        if (!userId.equals(project.getUserId())) return;
        projects.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public List<Project> findAll(final String userId) {
        final List<Project> result = new ArrayList<>();
        for (final Project project : projects) {
            if (userId.equals(project.getUserId())) result.add((project));
        }
        return result;
    }

    @Override
    public void clear(final String userId) {
        final List<Project> userProjects = findAll(userId);
        projects.removeAll(userProjects);
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public Project findById(final String userId, final String id) {
        for (final Project project : projects) {
            if (id.equals((project.getId()))) return project;
            if (id.equals((project.getId())) && userId.equals(project.getUserId()))
                return project;
        }
        return null;
    }

    @Override
    public Project findByIndex(final String userId, final Integer index) {
        final List<Project> userProjects = new ArrayList<>();
        for (final Project project : projects) {
            if (userId.equals(project.getUserId())) userProjects.add(project);
        }
        return userProjects.get(index);
    }

    @Override
    public Project findByName(final String userId, final String name) {
        for (final Project project : projects) {
            if (name.equals((project.getName())) && userId.equals(project.getUserId()))
                return project;
        }
        return null;
    }

    @Override
    public Project removeById(final String userId, final String id) {
        Project project = findById(userId, id);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

    @Override
    public Project removeByIndex(final String userId, final Integer index) {
        final Project project = findByIndex(userId, index);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

    @Override
    public Project removeByName(final String userId, final String name) {
        final Project project = findByName(userId, name);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

}