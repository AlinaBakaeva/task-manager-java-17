package com.bakaeva.tm.enumerated;

public enum Role {

    USER("User"),
    ADMIN("Administrator");

    private final String displayName;

    Role(final String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}